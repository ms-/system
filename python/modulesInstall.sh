# install the pip package

sudo pacman -S python-pip

# Python Packages
# 1. numpy
pip install numpy
# 2. scipy
pip install scipy
# 3. matplotlib
pip install matplotlib
# 4. xlrd
pip install xlrd
# 5. xlsxwriter
pip install xlsxwriter
# 6. cantera - errors while building