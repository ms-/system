# Python Setting Up using Miniconda : Windows 10

-----------------------------------------------------------------------
## Create new environment with specific python version

>> conda create -n "computation" python=3.7 ipython
>> conda create -n "codespace" python=3.7 ipython

-----------------------------------------------------------------------
## Package Installation

Spyder install >> conda install -c anaconda spyder

Numpy Install >> conda install -c anaconda numpy

Scipy Install >> conda install -c anaconda scipy

Cantera Install >> conda install --channel cantera cantera


Matplotlib >> conda install -c anaconda matplotlib

Xlrd Install >> conda install -c anaconda xlrd

Xlsxwriter >> conda install -c anaconda xlsxwriter

-----------------------------------------------------------------------
## Adding new module paths:

Windows: myRepository.pth
C:\ProgramData\Anaconda3\envs\computation\Lib\site-packages\myRepository.pth

Linux: myRepository.pth
/home/ms/Applications/miniconda3/envs/computation/lib/python3.7/site-packages/

## Adding Custom Matplotlib style file:

Windows:
C:\ProgramData\Anaconda3\envs\computation\Lib\site-packages\matplotlib\mpl-data\stylelib\presentation.mplstyle

Linux: presentation.mplstyle
/home/ms/Applications/miniconda3/envs/computation/lib/python3.7/site-packages/matplotlib/mpl-data/stylelib/

-----------------------------------------------------------------------
## Complete Anaconda Uninstall: 

https://docs.anaconda.com/anaconda/install/uninstall/

1. conda install anaconda-clean
2. anaconda-clean --yes
3. delete /envs and /pkgs folders
4. uninstall program from control panel

-----------------------------------------------------------------------

-----------------------------------------------------------------------
# pylint: disable=unused-import >> for diabling error in code analysis

# define cells >> #%%
